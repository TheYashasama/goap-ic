# GOAP IC
#### by Timo Neumann


##### Created with Unity 2018.2.6f1

Created for an Independant Coursework project at the University of Applied Sciences, Berlin.

Focus of this project will be usage of Goal Oriented Action Planning (GOAP).

The base application, created by following [Catlike Coding](https://catlikecoding.com/unity/tutorials/hex-map/), was expaneded with simple game mechaincs for a simple Resource Management game where a tribe of units has to survive and build castles.

The project will contrast the control of the tribe as one entitity as opposed to a tribe where each unit is controlled by itself. To that end, an GOAP (Goal Oriented Action Planning) System is used to control units.

Close game by pressing ESC.

Estimated finishing date: September 2018

---

Copyright (c) 2018 Timo Neumann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.