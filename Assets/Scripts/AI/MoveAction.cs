﻿using ReGoap.Core;
using ReGoap.Unity;
using System;
using System.Collections.Generic;

namespace GoapIC
{
    public class MoveAction : ReGoapAction<string, object>
    {
        HexGrid grid;
        WorkerUnit unit;

        protected override void Awake()
        {
            base.Awake();

            grid = FindObjectOfType<HexGrid>();
            unit = GetComponent<WorkerUnit>();
        }

        public override void Run(IReGoapAction<string, object> previous, IReGoapAction<string, object> next, ReGoapState<string, object> settings, ReGoapState<string, object> goalState, Action<IReGoapAction<string, object>> done, Action<IReGoapAction<string, object>> fail)
        {
            base.Run(previous, next, settings, goalState, done, fail);

            if (settings.HasKey("objectivePosition"))
            {
                HexCoordinates targetCoordinates = ((ResourceCell)settings.Get("objectivePosition")).coordinates;
                grid.FindPath(unit.Location, grid.GetCell(targetCoordinates), unit);
                unit.Travel(grid.GetMovePath(unit));
                if (unit.Location.coordinates.Equals(targetCoordinates))
                {
                    OnDoneMovement();
                }
                else
                {
                    OnFailureMovement();
                }
            }
            else
            {
                OnFailureMovement();
            }
        }

        public override bool CheckProceduralCondition(GoapActionStackData<string, object> stackData)
        {
            return base.CheckProceduralCondition(stackData) && stackData.settings.HasKey("objectivePosition");
        }

        public override ReGoapState<string, object> GetEffects(GoapActionStackData<string, object> stackData)
        {
            if (stackData.settings.HasKey("objectivePosition"))
            {
                effects.Set("isAtPosition", stackData.settings.Get("objectivePosition"));
            }
            else
            {
                effects.Clear();
            }
            return base.GetEffects(stackData);
        }

        public override List<ReGoapState<string, object>> GetSettings(GoapActionStackData<string, object> stackData)
        {
            if (stackData.goalState.HasKey("isAtPosition"))
            {
                settings.Set("objectivePosition", stackData.goalState.Get("isAtPosition"));
                return base.GetSettings(stackData);
            }

            return new List<ReGoapState<string, object>>();
        }

        public override float GetCost(GoapActionStackData<string, object> stackData)
        {
            var distance = 0.0f;
            if (stackData.settings.HasKey("objectivePosition") && stackData.currentState.HasKey("isAtPosition"))
            {
                ResourceCell objectivePosition = (ResourceCell)stackData.settings.Get("objectivePosition");
                ResourceCell isAtPosition = (ResourceCell)stackData.currentState.Get("isAtPosition");
                distance = objectivePosition.coordinates.DistanceTo(isAtPosition);
            }
            return base.GetCost(stackData) + Cost + distance;
        }

        protected virtual void OnFailureMovement()
        {
            failCallback(this);
        }

        protected virtual void OnDoneMovement()
        {
            doneCallback(this);
        }
    }
}