﻿using ReGoap.Core;
using ReGoap.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GoapIC
{
    public class GatherStoneAction : ReGoapAction<string, object>
    {
        [SerializeField] ResourcesObject resourcesObject;

        WorkerUnit unit;
        int neededStone;

        protected override void Awake()
        {
            base.Awake();
            unit = GetComponent<WorkerUnit>();
        }

        public override ReGoapState<string, object> GetPreconditions(GoapActionStackData<string, object> stackData)
        {
            preconditions.Clear();
            if (stackData.settings.HasKey("stoneCells"))
            {
                preconditions.Set("isAtPosition", stackData.settings.Get("stonePosition"));
            }
            return preconditions;
        }

        public override ReGoapState<string, object> GetEffects(GoapActionStackData<string, object> stackData)
        {
            effects.Clear();
            if (stackData.settings.HasKey("hasResourceStone"))
            {
                int previousCount = stackData.settings.HasKey("hasResourceStone") ? (int)stackData.settings.Get("hasResourceStone") : 0;
                effects.Set("hasResourceStone", previousCount + resourcesObject.GainAmount);
            }
            return effects;
        }

        public override List<ReGoapState<string, object>> GetSettings(GoapActionStackData<string, object> stackData)
        {
            settings.Clear();
            if (stackData.currentState.HasKey("stoneCells"))
            {
                ResourceCell target = new ResourceCell();
                var results = new List<ReGoapState<string, object>>();
                float bestScore = float.MaxValue;
                foreach (var stoneCell in (List<ResourceCell>)stackData.currentState.Get("stoneCells"))
                {
                    if (stoneCell.level <= 0) continue;

                    float score = stackData.currentState.HasKey("isAtPosition") ? stoneCell.coordinates.DistanceTo((HexCoordinates)stackData.currentState.Get("isAtPosition")) : 0.0f;
                    score += resourcesObject.FeatureMax - stoneCell.level;
                    if (score < bestScore)
                    {
                        bestScore = score;
                        target = stoneCell;
                    }
                }

                settings.Set("stonePosition", target);
                results.Add(settings.Clone());

                return results;
            }
            return new List<ReGoapState<string, object>>();
        }

        public override float GetCost(GoapActionStackData<string, object> stackData)
        {
            var extraCost = 0.0f;
            if (stackData.settings.HasKey("needStone"))
            {
                int neededStone = (int)stackData.settings.Get("needStone");
                extraCost += neededStone;
            }
            return base.GetCost(stackData) + extraCost;
        }

        public override bool CheckProceduralCondition(GoapActionStackData<string, object> stackData)
        {
            return base.CheckProceduralCondition(stackData) && stackData.settings.HasKey("stoneCells");
        }

        public override void Run(IReGoapAction<string, object> previous, IReGoapAction<string, object> next, ReGoapState<string, object> settings, ReGoapState<string, object> goalState, Action<IReGoapAction<string, object>> done, Action<IReGoapAction<string, object>> fail)
        {
            base.Run(previous, next, settings, goalState, done, fail);

            var thisSettings = settings;
            neededStone = (int)thisSettings.Get("needStone");

            if (neededStone < resourcesObject.GainAmount)
                failCallback(this);
            else
            {
                unit.Collect(resourcesObject);
            }
        }

        public override void PlanEnter(IReGoapAction<string, object> previousAction, IReGoapAction<string, object> nextAction, ReGoapState<string, object> settings, ReGoapState<string, object> goalState)
        {
            /*
            if (settings.HasKey("resource"))
            {
                ((IResource)settings.Get("resource")).Reserve(GetHashCode()); // TODO this prevents multiple units from targeting the same resource - should this also be implemented? This may not be needed, as units may need to act in sequence anyway
            }*/
        }
        public override void PlanExit(IReGoapAction<string, object> previousAction, IReGoapAction<string, object> nextAction, ReGoapState<string, object> settings, ReGoapState<string, object> goalState)
        {
            /*
            if (settings.HasKey("resource"))
            {
                ((IResource)settings.Get("resource")).Unreserve(GetHashCode());
            }*/
        }
    }
}