﻿using System;
using ReGoap.Core;
using ReGoap.Unity;
using UnityEngine;

namespace GoapIC
{
    public class StockFoodGoal : ReGoapGoal<string, object>
    {
        [SerializeField] ResourcesObject resourcesObject;
        [SerializeField] TurnObject turnObject;
        [SerializeField] int foodStock;
        [SerializeField] float priorityDecreaseIfStocked;

        float baseLinePriority;

        protected override void Awake()
        {
            base.Awake();
            goal.Set("collectedfood", true);
            baseLinePriority = Priority;
            turnObject.OnTurnEnd += RecalculatePriority;
        }

        private void RecalculatePriority()
        {
            Priority = resourcesObject.Food < foodStock ? baseLinePriority : baseLinePriority * priorityDecreaseIfStocked;
            Debug.Log("prio: " + Priority);
        }

        public override string ToString()
        {
            return string.Format("GoapGoal('{0}')", Name);
        }
    }
}

