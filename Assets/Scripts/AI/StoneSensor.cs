﻿namespace GoapIC
{
    public class StoneSensor : ResourceSensor
    {
        protected override int GetLevel(HexCell cell)
        {
            return cell.StoneLevel;
        }
    }
}