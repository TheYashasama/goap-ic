﻿using ReGoap.Core;
using ReGoap.Unity;
using UnityEngine;

namespace GoapIC
{
    public class PositionSensor : ReGoapSensor<string, object>
    {
        WorkerUnit unit;
        public override void Init(IReGoapMemory<string, object> memory)
        {
            base.Init(memory);
            unit = GetComponent<WorkerUnit>();
            UpdateSensor();
        }

        public override void UpdateSensor()
        {
            var state = memory.GetWorldState();
            if(unit.Location) state.Set("isAtPosition", new ResourceCell(unit.Location, 0));
        }
    }
}