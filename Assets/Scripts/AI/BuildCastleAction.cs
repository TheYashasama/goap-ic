﻿using ReGoap.Core;
using ReGoap.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GoapIC
{
    public class BuildCastleAction : ReGoapAction<string, object>
    {
        [SerializeField] ResourcesObject resourcesObject;

        WorkerUnit unit;
        int neededStone;

        protected override void Awake()
        {
            base.Awake();
            unit = GetComponent<WorkerUnit>();
        }

        public override ReGoapState<string, object> GetPreconditions(GoapActionStackData<string, object> stackData)
        {
            preconditions.Clear();

            preconditions.Set("canAffordCastle", true);
            return preconditions;
        }

        public override ReGoapState<string, object> GetEffects(GoapActionStackData<string, object> stackData)
        {
            effects.Clear();

            effects.Set("buildCastle", true);
            return effects;
        }

        public override List<ReGoapState<string, object>> GetSettings(GoapActionStackData<string, object> stackData)
        {
            settings.Clear();
            return new List<ReGoapState<string, object>>();
        }

        public override float GetCost(GoapActionStackData<string, object> stackData)
        {
            var extraCost = 0.0f;

            return base.GetCost(stackData) + extraCost;
        }

        public override bool CheckProceduralCondition(GoapActionStackData<string, object> stackData)
        {
            return base.CheckProceduralCondition(stackData);
        }

        public override void Run(IReGoapAction<string, object> previous, IReGoapAction<string, object> next, ReGoapState<string, object> settings, ReGoapState<string, object> goalState, Action<IReGoapAction<string, object>> done, Action<IReGoapAction<string, object>> fail)
        {
            base.Run(previous, next, settings, goalState, done, fail);

            if (unit.CanBuildCastle(resourcesObject))
            {
                unit.BuildCastle(resourcesObject);
            }
            else
            {
                failCallback(this);
            }
        }

        public override void PlanEnter(IReGoapAction<string, object> previousAction, IReGoapAction<string, object> nextAction, ReGoapState<string, object> settings, ReGoapState<string, object> goalState)
        {

        }
        public override void PlanExit(IReGoapAction<string, object> previousAction, IReGoapAction<string, object> nextAction, ReGoapState<string, object> settings, ReGoapState<string, object> goalState)
        {

        }
    }
}