﻿using ReGoap.Core;
using ReGoap.Unity;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace GoapIC
{
    public class GatherResourceAction : ReGoapAction<string, object>
    {
        [SerializeField] string resourceName;
        [SerializeField] ResourcesObject resourcesObject;
        [SerializeField] float distanceCostMultiplier = 4;

        WorkerUnit unit;
        int neededStone;

        protected override void Awake()
        {
            base.Awake();
            unit = GetComponent<WorkerUnit>();
        }

        public override ReGoapState<string, object> GetPreconditions(GoapActionStackData<string, object> stackData)
        {
            preconditions.Clear();

            preconditions.Set("isAtPosition", stackData.settings.Get(resourceName + "Position"));
            return preconditions;
        }

        public override ReGoapState<string, object> GetEffects(GoapActionStackData<string, object> stackData)
        {
            effects.Clear();

            effects.Set("collected" + resourceName, true);
            return effects;
        }

        public override List<ReGoapState<string, object>> GetSettings(GoapActionStackData<string, object> stackData)
        {
            settings.Clear();
            if (stackData.currentState.HasKey(resourceName + "Cells"))
            {
                ResourceCell target = new ResourceCell();
                var results = new List<ReGoapState<string, object>>();
                float bestScore = float.MaxValue;
                foreach (var resourceCell in (List<ResourceCell>)stackData.currentState.Get(resourceName + "Cells"))
                {
                    if (resourceCell.level <= 0) continue;

                    float score = stackData.currentState.HasKey("isAtPosition") ? resourceCell.coordinates.DistanceTo((ResourceCell)stackData.currentState.Get("isAtPosition")) : 0.0f;
                    score *= distanceCostMultiplier;
                    // also compare to startPosition
                    score += resourcesObject.FeatureMax - resourceCell.level;
                    if (score < bestScore)
                    {
                        bestScore = score;
                        target = resourceCell;
                    }
                }

                settings.Set(resourceName + "Position", target);
                results.Add(settings.Clone());

                return results;
            }
            return new List<ReGoapState<string, object>>();
        }

        public override float GetCost(GoapActionStackData<string, object> stackData)
        {
            var extraCost = 0.0f;
            if (stackData.settings.HasKey(resourceName + "Position")) extraCost += ((ResourceCell)stackData.settings.Get(resourceName + "Position")).reserved * 1;
            return base.GetCost(stackData) + extraCost;
        }

        public override bool CheckProceduralCondition(GoapActionStackData<string, object> stackData)
        {
            return base.CheckProceduralCondition(stackData) && stackData.settings.HasKey(resourceName + "Position");
        }

        public override void Run(IReGoapAction<string, object> previous, IReGoapAction<string, object> next, ReGoapState<string, object> settings, ReGoapState<string, object> goalState, Action<IReGoapAction<string, object>> done, Action<IReGoapAction<string, object>> fail)
        {
            base.Run(previous, next, settings, goalState, done, fail);

            if (unit.CanCollect)
            {
                unit.Collect(resourcesObject);
            }
            else
            {
                failCallback(this);
            }
        }

        public override void PlanEnter(IReGoapAction<string, object> previousAction, IReGoapAction<string, object> nextAction, ReGoapState<string, object> settings, ReGoapState<string, object> goalState)
        {
            if (settings.HasKey(resourceName + "Position"))
            {
                ResourceCell cell = (ResourceCell)settings.Get(resourceName + "Position");
                cell.reserved++;
                settings.Set(resourceName +"Position", cell);
            }
        }
        public override void PlanExit(IReGoapAction<string, object> previousAction, IReGoapAction<string, object> nextAction, ReGoapState<string, object> settings, ReGoapState<string, object> goalState)
        {
            if (settings.HasKey(resourceName + "Position"))
            {
                ResourceCell cell = (ResourceCell)settings.Get(resourceName + "Position");
                cell.reserved--;
                settings.Set(resourceName + "Position", cell);
            }
        }
    }
}