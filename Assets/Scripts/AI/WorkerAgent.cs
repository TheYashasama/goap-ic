﻿using System;
using ReGoap.Unity;
using UnityEngine;

namespace GoapIC
{
    public class WorkerAgent : ReGoapAgent<string, object>
    {
        [SerializeField] TurnObject turnObject;

        protected override void Awake()
        {
            base.Awake();
            turnObject.OnTurnStart += Plan;
        }

        private void Plan()
        {
            // TODO add condition to CalculateNewGoal() to not run if the unit has alrady moved and / or acted
            CalculateNewGoal();
        }

        // TODO better handle destroy (MoveAction)
    }
}
