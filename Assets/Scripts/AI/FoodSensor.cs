﻿namespace GoapIC
{
    public class FoodSensor : ResourceSensor
    {
        protected override int GetLevel(HexCell cell)
        {
            return cell.FarmLevel;
        }
    }
}