﻿namespace GoapIC
{
    public class WoodSensor : ResourceSensor
    {
        protected override int GetLevel(HexCell cell)
        {
            return cell.PlantLevel;
        }
    }

}

