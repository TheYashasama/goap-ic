﻿using ReGoap.Core;
using ReGoap.Unity;
using UnityEngine;

namespace GoapIC
{
    public class BuildCastleGoal : ReGoapGoal<string, object>
    {
        protected override void Awake()
        {
            base.Awake();
            goal.Set("buildCastle", true);
        }

        public override string ToString()
        {
            return string.Format("GoapGoal('{0}')", Name);
        }
    }
}

