﻿using System.Collections.Generic;
using ReGoap.Unity;
using UnityEngine;

namespace GoapIC
{
    public class ResourceSensor : ReGoapSensor<string, object>
    {
        [SerializeField] string stateString;
        [SerializeField] int featureIndex;
        List<ResourceCell> resourceCells;
        HexGrid grid;

        void Start()
        {
            grid = FindObjectOfType<HexGrid>();
            UpdateSensor();
        }

        public override void UpdateSensor()
        {
            base.UpdateSensor();

            resourceCells = new List<ResourceCell>();

            // TODO think about exploration
            foreach (var cell in grid.GetFeatureCells(featureIndex))
            {
                int level = GetLevel(cell);
                if (level <= 0) continue;

                resourceCells.Add(new ResourceCell(cell, level));
            }

            var worldState = memory.GetWorldState();
            worldState.Set(stateString, resourceCells);
        }

        protected virtual int GetLevel(HexCell cell)
        {
            return -1;
        }
    }
}