﻿namespace GoapIC
{
    public struct ResourceCell
    {
        public ResourceCell(HexCell cell, int cellLevel)
        {
            coordinates = cell.coordinates;
            reserved = 0;
            this.level = cellLevel;
        }

        public HexCoordinates coordinates;
        public int level;
        public int reserved;
    }
}


