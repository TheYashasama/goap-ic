﻿using System;
using ReGoap.Core;
using ReGoap.Unity;
using UnityEngine;

namespace GoapIC
{
    public class WorkerMemory : ReGoapMemory<string, object>
    {
        [SerializeField] TurnObject turnObject;
        private IReGoapSensor<string, object>[] sensors;
                
        protected override void Awake()
        {
            base.Awake();
            sensors = GetComponents<IReGoapSensor<string, object>>();
            foreach (var sensor in sensors)
            {
                sensor.Init(this);
            }
            turnObject.OnTurnStart += UpdateSensors;
        }

        private void UpdateSensors()
        {
            foreach (var sensor in sensors)
            {
                sensor.UpdateSensor();
            }
        }
    }
}
