﻿using ReGoap.Core;
using ReGoap.Unity;
using UnityEngine;

namespace GoapIC
{
    public class StockSensor : ReGoapSensor<string, object>
    {
        [SerializeField] ResourcesObject resourcesObject;
        WorkerUnit unit;

        public override void Init(IReGoapMemory<string, object> memory)
        {
            base.Init(memory);
            unit = GetComponent<WorkerUnit>();
            UpdateSensor();
        }

        public override void UpdateSensor()
        {
            var state = memory.GetWorldState();
            if (unit.CanBuildCastle(resourcesObject)) state.Set("canAffordCastle", true);
            else state.Remove("canAffordCastle");
            // TODO also add farms
        }
    }
}