﻿namespace GoapIC
{

    public class WorkerUnit : HexUnit
    {
        const int castleIndex = 1;

        public bool Moved { get; set; }
        public bool Acted { get; set; }

        public bool CanCollect
        {
            get
            {
                return Location.StoneLevel >= 1 || Location.PlantLevel >= 1 || Location.FarmLevel >= 1;
            }
        }

        public bool Collect(ResourcesObject resources)
        {
            if (!CanCollect) return false;

            if(Location.StoneLevel >= 1)
            {
                resources.Stone += resources.GainAmount;
                Location.StoneLevel--;
            }
            if (Location.PlantLevel >= 1)
            {
                resources.Wood += resources.GainAmount;
                Location.PlantLevel--;
            }

            if (Location.FarmLevel >= 1)
            {
                resources.Food += resources.GainAmount;
                Location.FarmLevel--;
            }
            Acted = true;
            return true;
        }

        public bool CanBuildFarm(ResourcesObject resourcesObject)
        {
            return resourcesObject.CanCoverFarm && !Location.IsSpecial;
        }

        public void BuildFarm(ResourcesObject resources)
        {
            if (!CanBuildFarm(resources)) return;
            resources.BuyFarm();
            Location.PlantLevel = 0;
            Location.StoneLevel = 0;
            Location.UrbanLevel = 0;
            Location.SpecialIndex = 0;

            Location.FarmLevel = resources.FeatureMax;
            Acted = true;
        }

        public bool CanBuildCastle(ResourcesObject resourcesObject)
        {
            return resourcesObject.CanCoverCastle;
        }

        public void BuildCastle(ResourcesObject resources)
        {
            if (!CanBuildCastle(resources)) return;
            resources.BuyCastle();
            Location.PlantLevel = 0;
            Location.StoneLevel = 0;
            Location.UrbanLevel = 0;
            Location.FarmLevel = 0;

            Location.SpecialIndex = castleIndex;
            Acted = true;
        }
    }
}
