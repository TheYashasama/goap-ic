﻿using UnityEngine;

namespace GoapIC
{
    [CreateAssetMenu(menuName = "GOAP-IC/Turn Object")]
    public class TurnObject : ScriptableObject
    {
        public delegate void TurnEvent();
        public event TurnEvent OnTurnStart;
        public event TurnEvent OnTurnEnd;
        public event TurnEvent OnGameStart;
        public event TurnEvent OnGameEnd;

        int turnNubmer;
        public int TurnNumber { get { return turnNubmer; } }

        public void Initialize()
        {
            turnNubmer = 0;
            OnGameStart?.Invoke();
        }

        public void NewTurn()
        {
            OnTurnEnd?.Invoke();
            turnNubmer++;
            OnTurnStart?.Invoke();
        }

        public void EndGame()
        {
            OnGameEnd?.Invoke();
        }
    }
}
