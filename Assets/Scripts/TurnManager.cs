﻿using UnityEngine;

namespace GoapIC
{
    public class TurnManager : MonoBehaviour
    {
        [SerializeField] SaveLoadMenu saveLoadMenu;
        [SerializeField] TurnObject turnObject;
        [SerializeField] ResourcesObject resourcesObject;
        [SerializeField] HexGrid grid;
        

        public void StartGame()
        {
            saveLoadMenu.LoadDefault();
            turnObject.Initialize();
            resourcesObject.Initialize();
            Shader.DisableKeyword("HEX_MAP_EDIT_MODE");
            grid.ResetVisibility();
            grid.ClearPath();
        }


        public void NewTurn()
        {
            turnObject.NewTurn();
        }


    }
}
