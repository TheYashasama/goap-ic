﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GoapIC
{

    public class GameOverPanel : MonoBehaviour
    {
        [SerializeField] Image background;
        [SerializeField] TextMeshProUGUI won;
        [SerializeField] TextMeshProUGUI lost;
        [SerializeField] Button restartGame;
        [SerializeField] TurnObject turnObject;
        [SerializeField] ResourcesObject resourcesObject;

        private void OnEnable()
        {
            turnObject.OnGameStart += Hide;
            turnObject.OnGameEnd += EndGame;
        }

        private void OnDisable()
        {
            turnObject.OnGameStart -= Hide;
            turnObject.OnGameEnd -= EndGame;
        }

        public void EndGame()
        {
            if (resourcesObject.Won) Win();
            else Lose();
        }

        public void Hide()
        {
            background.gameObject.SetActive(false);
            lost.gameObject.SetActive(false);
            won.gameObject.SetActive(false);
            restartGame.gameObject.SetActive(false);
        }

        public void Show()
        {
            background.gameObject.SetActive(true);
        }

        public void Win()
        {
            won.gameObject.SetActive(true);
            lost.gameObject.SetActive(false);
            restartGame.gameObject.SetActive(true);
            Show();
        }

        public void Lose()
        {
            lost.gameObject.SetActive(true);
            won.gameObject.SetActive(false);
            restartGame.gameObject.SetActive(true);
            Show();
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
