﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GoapIC
{
    public class ResourceUI : MonoBehaviour
    {

        [SerializeField] HexGrid hexGrid;
        [SerializeField] TurnObject turnObject;
        [SerializeField] ResourcesObject resourcesObject;
        [SerializeField] List<TextMeshProUGUI> resourceTexts;


        private void OnEnable()
        {
            turnObject.OnTurnStart += UpdateResources;
            resourcesObject.OnResourceUpdated += UpdateResources;
        }

        private void OnDisable()
        {
            turnObject.OnTurnStart -= UpdateResources;
            resourcesObject.OnResourceUpdated -= UpdateResources;
        }

        public void UpdateResources()
        {
            resourceTexts[0].text = "Units: " + hexGrid.NumberOfUnits;
            resourceTexts[1].text = "Points: " + resourcesObject.Points;
            resourceTexts[2].text = "Wood: " + resourcesObject.Wood;
            resourceTexts[3].text = "Stone: " + resourcesObject.Stone;
            resourceTexts[4].text = "Food: " + resourcesObject.Food;
            resourceTexts[5].text = "Turn: " + turnObject.TurnNumber;

        }
    }
}
