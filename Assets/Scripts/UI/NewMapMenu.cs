﻿using UnityEngine;
using UnityEngine.UI;

public class NewMapMenu : MonoBehaviour {

    public Image Background;

    public HexGrid hexGrid;
    public HexMapGenerator mapGenerator;
    bool generateMaps = true;
    bool wrapping = true;

    public void ToggleMapGeneration(bool toggle)
    {
        generateMaps = toggle;
    }
    
    public void ToggleWrapping(bool toggle)
    {
        wrapping = toggle;
    }

	public void Open () {
		gameObject.SetActive(true);
		HexMapCamera.Locked = true;
        Background?.gameObject.SetActive(true);
    }

	public void Close () {
		gameObject.SetActive(false);
		HexMapCamera.Locked = false;
        Background?.gameObject.SetActive(false);
    }

	public void CreateSmallMap () {
		CreateMap(20, 15);
	}

	public void CreateMediumMap () {
		CreateMap(40, 30);
	}

	public void CreateLargeMap () {
		CreateMap(80, 60);
	}

	void CreateMap (int x, int z) {
        if(generateMaps)
        {
            mapGenerator.GenerateMap(x, z, wrapping);
        } else
        {
		    hexGrid.CreateMap(x, z, wrapping);
        }
		HexMapCamera.ValidatePosition();
        Shader.DisableKeyword("HEX_MAP_EDIT_MODE"); // TODO properly integrate this, always start in discovery (play) mode
        Close();
	}
}