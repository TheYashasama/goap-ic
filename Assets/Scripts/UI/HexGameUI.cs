﻿using GoapIC;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HexGameUI : MonoBehaviour
{
    public HexGrid grid;
    [SerializeField] TurnObject turnObject;
    [SerializeField] ResourcesObject resourcesObject;
    [SerializeField] RectTransform unitUI;
    Toggle movedToggle, actedToggle;
    Button collectButton, buildFarmButton, buildCastleButton;

    HexCell currentCell;

    HexUnit selectedUnit;

    WorkerUnit Worker
    {
        get
        {
            return selectedUnit as WorkerUnit;
        }
    }

    public void SetEditMode(bool toggle)
    {
        enabled = !toggle;
        grid.ShowUI(!toggle);
        grid.ClearPath();
    }

    private void OnEnable()
    {
        Toggle[] toggles = unitUI.GetComponentsInChildren<Toggle>();
        if (toggles.Length != 2)
        {
            Debug.LogError("There should be two toggles in the UnitUI");
            return;
        }
        movedToggle = toggles[0];
        actedToggle = toggles[1];
        Button[] unitButtons = unitUI.GetComponentsInChildren<Button>();
        if (unitButtons.Length != 3)
        {
            Debug.LogError("There should be two buttons in the UnitUI");
            return;
        }
        collectButton = unitButtons[0];
        buildFarmButton = unitButtons[1];
        buildCastleButton = unitButtons[2];

        collectButton.onClick.AddListener(Collect);
        buildFarmButton.onClick.AddListener(BuildFarm);
        buildCastleButton.onClick.AddListener(BuildCastle);

        turnObject.OnTurnStart += grid.RefreshUnits;
        turnObject.OnTurnStart += UpdateUnitUI;
        turnObject.OnTurnEnd += PayUnitUpkeep;
        UpdateUnitUI();
    }

    private void OnDisable()
    {
        turnObject.OnTurnStart -= grid.RefreshUnits;
        turnObject.OnTurnStart -= UpdateUnitUI;
        turnObject.OnTurnEnd -= PayUnitUpkeep;
    }

    private void UpdateUnitUI()
    {
        if (Worker)
        {
            movedToggle.isOn = Worker.Moved;
            actedToggle.isOn = Worker.Acted;
            collectButton.interactable = !Worker.Acted && Worker.CanCollect;
            buildFarmButton.interactable = !Worker.Acted && Worker.CanBuildFarm(resourcesObject);
            buildCastleButton.interactable = !Worker.Acted && Worker.CanBuildCastle(resourcesObject);
        }
        unitUI.gameObject.SetActive(Worker);
    }

    private void Collect()
    {
        if (!Worker) return;

        Worker.Collect(resourcesObject);
        UpdateUnitUI();
    }

    private void BuildFarm()
    {
        if (!Worker) return;

        Worker.BuildFarm(resourcesObject);
        UpdateUnitUI();
    }

    private void BuildCastle()
    {
        if (!Worker) return;

        Worker.BuildCastle(resourcesObject);
        resourcesObject.Points++;

        if (resourcesObject.Won) GameOver();
        UpdateUnitUI();
    }

    private void PayUnitUpkeep()
    {
        HexUnit[] units = grid.Units.ToArray();
        foreach (var unit in units)
        {
            if (resourcesObject.CanCoverUnit) resourcesObject.PayForUnit();
            else grid.RemoveUnit(unit);
        }
        if (grid.NumberOfUnits == 0) GameOver();
    }

    private void GameOver()
    {
        turnObject.EndGame();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Quit();

        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                DoSelection();
            }
            else if (selectedUnit)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    DoMove();
                }
                else
                {
                    DoPathfinding();
                }
            }
        }
    }

    void DoSelection()
    {
        grid.ClearPath();
        UpdateCurrentCell();
        if (currentCell)
        {
            selectedUnit = currentCell.Unit;
            currentCell.EnableHighlight(Color.blue);
        }
        UpdateUnitUI();
    }

    void DoPathfinding()
    {
        if (UpdateCurrentCell())
        {
            if (currentCell && selectedUnit.IsValidDestination(currentCell))
            {
                grid.FindPath(selectedUnit.Location, currentCell, selectedUnit);
            }
            else
            {
                grid.ClearPath();
            }
        }
    }

    void DoMove()
    {
        if (grid.HasPath && Worker && !Worker.Moved)
        {
            selectedUnit.Travel(grid.GetMovePath(selectedUnit));
            Worker.Moved = true;
            UpdateUnitUI();
            grid.ClearPath();
        }
    }

    bool UpdateCurrentCell()
    {
        HexCell cell = grid.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
        if (cell != currentCell)
        {
            if (currentCell) currentCell.DisableHighlight();
            currentCell = cell;
            return true;
        }
        return false;
    }

    void Quit()
    {
        Application.Quit();
    }
}