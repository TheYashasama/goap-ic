﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMapGenerator : MonoBehaviour
{
    const int CLIMATE_CYCLES = 40;

    struct ClimateData
    {
        public float clouds, moisture;
    }

    struct MapRegion
    {
        public int xMin, xMax, zMin, zMax;
    }
    struct Biome
    {
        public int terrain, plant;

        public Biome(int terrain, int plant)
        {
            this.terrain = terrain;
            this.plant = plant;
        }
    }

    public enum HemisphereMode
    {
        Both, North, South
    }

    static float[] temperatureBands = { 0.1f, 0.3f, 0.6f };
    static float[] moistureBands = { 0.12f, 0.28f, 0.85f };
    static Biome[] biomes = {
        new Biome(0, 0), new Biome(4, 0), new Biome(4, 0), new Biome(4, 0),
        new Biome(0, 0), new Biome(2, 0), new Biome(2, 1), new Biome(2, 2),
        new Biome(0, 0), new Biome(1, 0), new Biome(1, 1), new Biome(1, 2),
        new Biome(0, 0), new Biome(1, 1), new Biome(1, 2), new Biome(1, 3)
    };

    public HexGrid Grid;
    public bool UseFixedSeed;
    public int Seed;

    [Header("Basic Settings")]
    [Range(20, 200)] public int ChunkSizeMin = 30;
    [Range(20, 200)] public int ChunkSizeMax = 100;
    [Range(5, 95)] public int LandPercentage = 50;
    [Range(1, 5)] public int WaterLevel = 3;
    [Range(-4, 0)] public int ElevationMinimum = -2;
    [Range(6, 10)] public int ElevationMaximum = 8;
    [Range(0, 100)] public int ErosionPercentage = 50;

    [Header("Probability Settings")]
    [Range(0f, 0.5f)] public float JitterProbability = 0.25f;
    [Range(0f, 1f)] public float HighRiseProbability = 0.25f;
    [Range(0f, 0.4f)] public float SinkProbabilty = 0.2f;

    [Header("Border Settings")]
    [Range(0, 10)] public int MapBorderX = 5;
    [Range(0, 10)] public int MapBorderZ = 5;

    [Header("Region Settings")]
    [Range(0, 10)] public int RegionBorder = 5;
    [Range(1, 4)] public int RegionCount = 1;

    [Header("Climate Settings")]
    [Range(0f, 1f)] public float StartingMoisture = 0.1f;
    [Range(0f, 1f)] public float EvaporationFactor = 0.5f;
    [Range(0f, 1f)] public float PrecipitationFactor = 0.25f;
    [Range(0f, 1f)] public float RunoffFactor = 0.25f;
    [Range(0f, 1f)] public float SeepageFactor = 0.125f;
    public HexDirection WindDirection = HexDirection.NW;
    [Range(1f, 10f)] float WindStrength = 4f;

    [Header("Biome Settings")]
    [Range(0, 20)] public int RiverPercentage = 10;
    [Range(0f, 1f)] public float ExtraLakeProbability = 0.25f;

    [Header("Temperature Settings")]
    [Range(0f, 1f)] public float LowTemperature = 0f;
    [Range(0f, 1f)] public float HighTemperature = 1f;
    public HemisphereMode Hemisphere;
    [Range(0f, 1f)] public float TemperatureJitter = 0.1f;

    List<MapRegion> regions = new List<MapRegion>();
    List<ClimateData> climate = new List<ClimateData>();
    List<ClimateData> nextClimate = new List<ClimateData>();
    List<HexDirection> flowDirections = new List<HexDirection>();

    int cellCount, landCells;
    HexCellPriorityQueue searchFrontier;
    int searchFrontierPhase;
    int temperatureJitterChannel;
    int BorderX
    {
        get
        {
            return Grid.wrapping ? RegionBorder : MapBorderX;
        }
    }

    public void GenerateMap(int x, int z, bool wrapping)
    {
        Random.State originalRandomState = Random.state;
        if (!UseFixedSeed)
        {
            Seed = Random.Range(0, int.MaxValue);
            Seed ^= (int)System.DateTime.Now.Ticks;
            Seed ^= (int)Time.unscaledTime;
            Seed &= int.MaxValue;
        }
        Random.InitState(Seed);
        cellCount = x * z;
        Grid.CreateMap(x, z, wrapping);

        if (searchFrontier == null)
        {
            searchFrontier = new HexCellPriorityQueue();
        }
        for (int i = 0; i < cellCount; i++)
        {
            Grid.GetCell(i).WaterLevel = WaterLevel;
        }

        CreateRegions();
        CreateLand();
        ErodeLand();
        CreateClimate();
        CreateRivers();
        SetTerrainTypes();
        for (int i = 0; i < cellCount; i++)
        {
            Grid.GetCell(i).SearchPhase = 0;
        }
        Random.state = originalRandomState;
    }

    void CreateRegions()
    {
        if (regions == null)
        {
            regions = new List<MapRegion>();
        }
        else
        {
            regions.Clear();
        }
        switch (RegionCount)
        {
            default:
                CreateOneRegion();
                break;
            case 2:
                CreateTwoRegions();
                break;
            case 3:
                CreateThreeRegions();
                break;
            case 4:
                CreateFourRegions();
                break;

        }
    }

    void CreateFourRegions()
    {
        MapRegion region;
        region.xMin = BorderX;
        region.xMax = Grid.cellCountX / 2 - RegionBorder;
        region.zMin = MapBorderZ;
        region.zMax = Grid.cellCountZ / 2 - RegionBorder;
        regions.Add(region);
        region.xMin = Grid.cellCountX / 2 + RegionBorder;
        region.xMax = Grid.cellCountX - BorderX;
        regions.Add(region);
        region.zMin = Grid.cellCountZ / 2 + RegionBorder;
        region.zMax = Grid.cellCountZ - MapBorderZ;
        regions.Add(region);
        region.xMin = BorderX;
        region.xMax = Grid.cellCountX / 2 - RegionBorder;
        regions.Add(region);
    }

    private void CreateThreeRegions()
    {
        MapRegion region;
        region.xMin = BorderX;
        region.xMax = Grid.cellCountX / 3 - RegionBorder;
        region.zMin = MapBorderZ;
        region.zMax = Grid.cellCountZ - MapBorderZ;
        regions.Add(region);
        region.xMin = Grid.cellCountX / 3 + RegionBorder;
        region.xMax = Grid.cellCountX * 2 / 3 - RegionBorder;
        regions.Add(region);
        region.xMin = Grid.cellCountX * 2 / 3 + RegionBorder;
        region.xMax = Grid.cellCountX - BorderX;
        regions.Add(region);
    }

    private void CreateTwoRegions()
    {
        MapRegion region;
        if (Random.value < 0.5f)
        {
            region.xMin = BorderX;
            region.xMax = Grid.cellCountX / 2 - RegionBorder;
            region.zMin = MapBorderZ;
            region.zMax = Grid.cellCountZ - MapBorderZ;
            regions.Add(region);
            region.xMin = Grid.cellCountX / 2 + RegionBorder;
            region.xMax = Grid.cellCountX - BorderX;
            regions.Add(region);
        }
        else
        {
            region.xMin = Grid.wrapping ? 0 : BorderX;
            region.xMax = Grid.cellCountX - (Grid.wrapping ? 0 : BorderX);
            region.zMin = MapBorderZ;
            region.zMax = Grid.cellCountZ / 2 - RegionBorder;
            regions.Add(region);
            region.zMin = Grid.cellCountZ / 2 + RegionBorder;
            region.zMax = Grid.cellCountZ - MapBorderZ;
            regions.Add(region);
        }
    }

    private void CreateOneRegion()
    {
        MapRegion region;
        region.xMin = Grid.wrapping ? 0 : BorderX;
        region.xMax = Grid.cellCountX - (Grid.wrapping ? 0 : BorderX);
        region.zMin = MapBorderZ;
        region.zMax = Grid.cellCountZ - MapBorderZ;
        regions.Add(region);
    }

    private void CreateLand()
    {
        int landBudget = Mathf.RoundToInt(cellCount * LandPercentage * 0.01f);
        landCells = landBudget;
        for (int guard = 0; guard < 10000; guard++)
        {
            bool sink = Random.value < SinkProbabilty;
            for (int i = 0; i < regions.Count; i++)
            {
                MapRegion region = regions[i];
                int chunkSize = Random.Range(ChunkSizeMin, ChunkSizeMax + 1);
                if (sink)
                {
                    landBudget = SinkTerrain(chunkSize, landBudget, region);
                }
                else
                {
                    landBudget = RaiseTerrain(chunkSize, landBudget, region);
                    if (landBudget == 0) return;
                }
            }
        }

        if (landBudget > 0)
        {
            Debug.LogWarning($"Failed to use up {landBudget} of the land budget.");
            landCells -= landBudget;
        }
    }

    int RaiseTerrain(int chunkSize, int budget, MapRegion region)
    {
        searchFrontierPhase += 1;
        HexCell firstCell = GetRandomCell(region);
        firstCell.SearchPhase = searchFrontierPhase;
        firstCell.Distance = 0;
        firstCell.SearchHeuristic = 0;
        searchFrontier.Enqueue(firstCell);
        HexCoordinates center = firstCell.coordinates;
        int rise = Random.value < HighRiseProbability ? 2 : 1;
        int size = 0;
        while (size < chunkSize && searchFrontier.Count > 0)
        {
            HexCell current = searchFrontier.Dequeue();
            int originalElevation = current.Elevation;
            int newElevation = originalElevation + rise;
            if (newElevation > ElevationMaximum)
            {
                continue;
            }
            current.Elevation = newElevation;
            bool becameLand = originalElevation < WaterLevel && newElevation >= WaterLevel;
            if (becameLand && --budget == 0)
            {
                break;
            }
            size += 1;

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                HexCell neighbor = current.GetNeighbor(d);
                if (neighbor && neighbor.SearchPhase < searchFrontierPhase)
                {
                    neighbor.SearchPhase = searchFrontierPhase;
                    neighbor.Distance = neighbor.coordinates.DistanceTo(center);
                    neighbor.SearchHeuristic = Random.value < JitterProbability ? 1 : 0;
                    searchFrontier.Enqueue(neighbor);
                }
            }
        }
        searchFrontier.Clear();
        return budget;
    }

    int SinkTerrain(int chunkSize, int budget, MapRegion region)
    {
        searchFrontierPhase += 1;
        HexCell firstCell = GetRandomCell(region);
        firstCell.SearchPhase = searchFrontierPhase;
        firstCell.Distance = 0;
        firstCell.SearchHeuristic = 0;
        searchFrontier.Enqueue(firstCell);
        HexCoordinates center = firstCell.coordinates;
        int sink = Random.value < HighRiseProbability ? 2 : 1;
        int size = 0;
        while (size < chunkSize && searchFrontier.Count > 0)
        {
            HexCell current = searchFrontier.Dequeue();
            int originalElevation = current.Elevation;
            int newElevation = originalElevation - sink;
            if (newElevation < ElevationMinimum)
            {
                continue;
            }
            current.Elevation = newElevation;
            bool lostLand = originalElevation >= WaterLevel && newElevation < WaterLevel;
            if (lostLand)
            {
                budget += 1;
            }
            size += 1;

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                HexCell neighbor = current.GetNeighbor(d);
                if (neighbor && neighbor.SearchPhase < searchFrontierPhase)
                {
                    neighbor.SearchPhase = searchFrontierPhase;
                    neighbor.Distance = neighbor.coordinates.DistanceTo(center);
                    neighbor.SearchHeuristic = Random.value < JitterProbability ? 1 : 0;
                    searchFrontier.Enqueue(neighbor);
                }
            }
        }
        searchFrontier.Clear();
        return budget;
    }

    void ErodeLand()
    {
        List<HexCell> erodibleCells = ListPool<HexCell>.Get();
        for (int i = 0; i < cellCount; i++)
        {
            HexCell cell = Grid.GetCell(i);
            if (IsErodible(cell))
            {
                erodibleCells.Add(cell);
            }
        }

        int targetErodibleCount = (int)(erodibleCells.Count * (100 - ErosionPercentage) * 0.01f);

        while (erodibleCells.Count > targetErodibleCount)
        {
            int index = Random.Range(0, erodibleCells.Count);
            HexCell cell = erodibleCells[index];
            HexCell targetCell = GetErosionTarget(cell);

            cell.Elevation -= 1;
            targetCell.Elevation += 1;

            if (!IsErodible(cell))
            {
                erodibleCells[index] = erodibleCells[erodibleCells.Count - 1];
                erodibleCells.RemoveAt(erodibleCells.Count - 1);
            }

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                HexCell neighbor = cell.GetNeighbor(d);
                if (neighbor && neighbor.Elevation == cell.Elevation + 2 && !erodibleCells.Contains(neighbor))
                {
                    erodibleCells.Add(neighbor);
                }
            }

            if (IsErodible(targetCell) && !erodibleCells.Contains(targetCell))
            {
                erodibleCells.Add(targetCell);
            }

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                HexCell neighbor = targetCell.GetNeighbor(d);
                if (neighbor && neighbor != cell && !IsErodible(neighbor) &&
                    neighbor.Elevation == targetCell.Elevation + 1)
                {
                    erodibleCells.Remove(neighbor);
                }
            }
        }

        ListPool<HexCell>.Add(erodibleCells);
    }

    void SetTerrainTypes()
    {
        temperatureJitterChannel = Random.Range(0, 4);
        int rockDesertElevation = ElevationMaximum - (ElevationMaximum - WaterLevel) / 2;
        for (int i = 0; i < cellCount; i++)
        {
            HexCell cell = Grid.GetCell(i);
            float temperature = DetermineTemerature(cell);
            float moisture = climate[i].moisture;
            if (!cell.IsUnderwater)
            {
                int t = 0;
                for (; t < temperatureBands.Length; t++)
                {
                    if (temperature < temperatureBands[t]) break;
                }
                int m = 0;
                for (; m < moistureBands.Length; m++)
                {
                    if (moisture < moistureBands[m]) break;
                }
                Biome cellBiome = biomes[t * 4 + m];
                if (cellBiome.terrain == 0)
                {
                    if (cell.Elevation >= rockDesertElevation) cellBiome.terrain = 3;
                }
                else if (cell.Elevation == ElevationMaximum)
                {
                    cellBiome.terrain = 4;
                }

                if (cellBiome.terrain == 4)
                {
                    cellBiome.plant = 0;
                }
                else if (cellBiome.plant < 3 && cell.HasRiver)
                {
                    cellBiome.plant += 1;
                }
                cell.TerrainTypeIndex = cellBiome.terrain;
                cell.PlantLevel = cellBiome.plant;
            }
            else
            {
                int terrain;
                if (cell.Elevation == WaterLevel - 1)
                {
                    int cliffs = 0, slopes = 0;
                    for (HexDirection d = HexDirection.NE; d < HexDirection.NW; d++)
                    {
                        HexCell neighbour = cell.GetNeighbor(d);
                        if (!neighbour) continue;

                        int delta = neighbour.Elevation - cell.WaterLevel;
                        if (delta == 0) slopes += 1;
                        else if (delta > 0) cliffs += 1;
                    }

                    if (cliffs + slopes > 3) terrain = 1;
                    else if (cliffs > 0) terrain = 3;
                    else if (slopes > 0) terrain = 0;
                    else terrain = 1;
                }
                else if (cell.Elevation >= WaterLevel)
                {
                    terrain = 1;
                } else if (cell.Elevation < 0)
                {
                    terrain = 3;
                } else
                {
                    terrain = 2;
                }

                if (terrain == 1 && temperature < temperatureBands[0]) terrain = 2;

                cell.TerrainTypeIndex = terrain;
            }
        }
    }

    HexCell GetRandomCell(MapRegion region)
    {
        return Grid.GetCell(Random.Range(region.xMin, region.xMax), Random.Range(region.zMin, region.zMax));
    }

    bool IsErodible(HexCell cell)
    {
        int erodibleElevation = cell.Elevation - 2;
        for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
        {
            HexCell neighbor = cell.GetNeighbor(d);
            if (neighbor && neighbor.Elevation <= erodibleElevation) return true;
        }
        return false;
    }

    HexCell GetErosionTarget(HexCell cell)
    {
        List<HexCell> candidates = ListPool<HexCell>.Get();
        int erodibleElevation = cell.Elevation - 2;
        for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
        {
            HexCell neighbor = cell.GetNeighbor(d);
            if (neighbor && neighbor.Elevation <= erodibleElevation)
            {
                candidates.Add(neighbor);
            }
        }
        HexCell target = candidates[Random.Range(0, candidates.Count)];
        ListPool<HexCell>.Add(candidates);
        return target;
    }

    void CreateClimate()
    {
        climate.Clear();
        nextClimate.Clear();
        ClimateData initialData = new ClimateData();
        ClimateData clearData = new ClimateData();
        initialData.moisture = StartingMoisture;
        for (int i = 0; i < cellCount; i++)
        {
            climate.Add(initialData);
            nextClimate.Add(clearData);
        }
        for (int cycle = 0; cycle < CLIMATE_CYCLES; cycle++)
        {
            for (int i = 0; i < cellCount; i++)
            {
                EvolveClimate(i);
            }
            List<ClimateData> swap = climate;
            climate = nextClimate;
            nextClimate = swap;
        }
    }

    void EvolveClimate(int cellIndex)
    {
        HexCell cell = Grid.GetCell(cellIndex);
        ClimateData cellClimate = climate[cellIndex];

        if (cell.IsUnderwater)
        {
            cellClimate.moisture = 1f;
            cellClimate.clouds += EvaporationFactor;
        }
        else
        {
            float evaporation = cellClimate.moisture * EvaporationFactor;
            cellClimate.moisture -= evaporation;
            cellClimate.clouds += evaporation;
        }

        float precipitation = cellClimate.clouds * PrecipitationFactor;
        cellClimate.clouds -= precipitation;
        cellClimate.moisture += precipitation;

        float cloudMaximum = 1f - cell.ViewElevation / (ElevationMaximum + 1f);
        if (cellClimate.clouds > cloudMaximum)
        {
            cellClimate.moisture += cellClimate.clouds - cloudMaximum;
            cellClimate.clouds = cloudMaximum;
        }

        HexDirection mainDispersalDirection = WindDirection.Opposite();
        float cloudDispersal = cellClimate.clouds * (1f / (5f + WindStrength));
        float runoff = cellClimate.moisture * RunoffFactor * (1f / 6f);
        float seepage = cellClimate.moisture * SeepageFactor * (1f / 6f);
        for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
        {
            HexCell neighbor = cell.GetNeighbor(d);
            if (!neighbor)
            {
                continue;
            }
            ClimateData neighborClimateData = nextClimate[neighbor.Index];

            if (d == mainDispersalDirection)
            {
                neighborClimateData.clouds += cloudDispersal * WindStrength;
            }
            else
            {
                neighborClimateData.clouds += cloudDispersal;
            }

            int elevationDelta = neighbor.ViewElevation - cell.ViewElevation;
            if (elevationDelta < 0)
            {
                cellClimate.moisture -= runoff;
                neighborClimateData.moisture += runoff;
            }
            else if (elevationDelta == 0)
            {
                cellClimate.moisture -= seepage;
                neighborClimateData.moisture += seepage;
            }

            nextClimate[neighbor.Index] = neighborClimateData;
        }
        ClimateData nextClimateData = nextClimate[cellIndex];
        nextClimateData.moisture += Mathf.Clamp(cellClimate.moisture, 0, 1f);
        nextClimate[cellIndex] = nextClimateData;

        climate[cellIndex] = new ClimateData();
    }

    void CreateRivers()
    {
        List<HexCell> riverOrigins = ListPool<HexCell>.Get();
        for (int i = 0; i < cellCount; i++)
        {
            HexCell cell = Grid.GetCell(i);
            if (cell.IsUnderwater) continue;

            ClimateData data = climate[i];
            float weight = data.moisture * (cell.Elevation - WaterLevel) / (ElevationMaximum - WaterLevel);
            if (weight > 0.75f)
            {
                riverOrigins.Add(cell);
                riverOrigins.Add(cell);
            }
            if (weight > 0.5f)
            {
                riverOrigins.Add(cell);
            }
            if (weight > 0.25f)
            {
                riverOrigins.Add(cell);
            }
        }
        int riverBudget = Mathf.RoundToInt(landCells * RiverPercentage * 0.01f);
        while (riverBudget > 0 && riverOrigins.Count > 0)
        {
            int index = Random.Range(0, riverOrigins.Count);
            int lastIndex = riverOrigins.Count - 1;
            HexCell origin = riverOrigins[index];
            riverOrigins[index] = riverOrigins[lastIndex];
            riverOrigins.RemoveAt(lastIndex);

            if (!origin.HasRiver)
            {
                bool isValidOrigin = true;
                for (HexDirection d = HexDirection.NE; d < HexDirection.NW; d++)
                {
                    HexCell neighbor = origin.GetNeighbor(d);
                    if (neighbor && (neighbor.HasRiver || neighbor.IsUnderwater))
                    {
                        isValidOrigin = false;
                        break;
                    }
                }
                if (isValidOrigin)
                {
                    riverBudget -= CreateRiver(origin);
                }
            }
        }

        if (riverBudget > 0)
        {
            Debug.LogWarning("Failed to use up river budget.");
        }


        ListPool<HexCell>.Add(riverOrigins);
    }

    int CreateRiver(HexCell origin)
    {
        int length = 1;

        HexCell cell = origin;
        HexDirection direction = HexDirection.NE;
        while (!cell.IsUnderwater)
        {
            int minNeighborsElevation = int.MaxValue;
            flowDirections.Clear();
            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                HexCell neighbor = cell.GetNeighbor(d);
                if (!neighbor) continue;

                if (neighbor.Elevation < minNeighborsElevation)
                {
                    minNeighborsElevation = neighbor.Elevation;
                }

                if (neighbor == origin || neighbor.HasIncomingRiver) continue;

                int elevationDelta = neighbor.Elevation - cell.Elevation;
                if (elevationDelta > 0) continue;

                if (neighbor.HasOutgoingRiver)
                {
                    cell.SetOutgoingRiver(d);
                    return length;
                }

                if (elevationDelta < 0)
                {
                    flowDirections.Add(d);
                    flowDirections.Add(d);
                    flowDirections.Add(d);
                }

                if (length == 1 || (d != direction.Next2() && d != direction.Previous2()))
                {
                    flowDirections.Add(d);
                }

                flowDirections.Add(d);
            }
            if (flowDirections.Count == 0)
            {
                if (length == 1)
                {
                    return 0;
                }

                if (minNeighborsElevation >= cell.Elevation)
                {
                    cell.WaterLevel = minNeighborsElevation;
                    if (minNeighborsElevation == cell.Elevation)
                    {
                        cell.Elevation = minNeighborsElevation - 1;
                    }
                }
                break;
            }

            direction = flowDirections[Random.Range(0, flowDirections.Count)];
            cell.SetOutgoingRiver(direction);
            length += 1;

            if (minNeighborsElevation >= cell.Elevation && Random.value < ExtraLakeProbability)
            {
                cell.WaterLevel = cell.Elevation;
                cell.Elevation -= 1;
            }

            cell = cell.GetNeighbor(direction);
        }

        return length;
    }

    float DetermineTemerature(HexCell cell)
    {
        float latitude = (float)cell.coordinates.Z / Grid.cellCountZ;
        if (Hemisphere == HemisphereMode.Both)
        {
            latitude *= 2f;
            if (latitude > 1f)
            {
                latitude = 2f - latitude;
            }
        }
        else if (Hemisphere == HemisphereMode.North)
        {
            latitude = 1f - latitude;
        }

        float noiseSample = HexMetrics.SampleNoise(cell.Position * 0.1f)[temperatureJitterChannel];
        float temperature = Mathf.LerpUnclamped(LowTemperature, HighTemperature, latitude);
        temperature *= 1f - (cell.ViewElevation - WaterLevel) / (ElevationMaximum - WaterLevel + 1f);
        temperature += (noiseSample * 2f - 1f) * TemperatureJitter;
        return temperature;
    }
}
