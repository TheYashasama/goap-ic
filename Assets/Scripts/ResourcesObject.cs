﻿using UnityEngine;

namespace GoapIC
{
    [CreateAssetMenu(menuName = "GOAP-IC/Resources Object")]
    public class ResourcesObject : ScriptableObject
    {
        public delegate void ResourceEvent();
        public event ResourceEvent OnResourceUpdated;

        [SerializeField] int gainAmount;
        public int GainAmount { get { return gainAmount; } }
        [SerializeField] int featureMax;
        public int FeatureMax { get { return featureMax; } }
        [SerializeField] int farmCost;
        [SerializeField] int castleCost;
        [SerializeField] int unitUpkeep;
        [SerializeField] int pointsForWin;

        [SerializeField] int startingWood;
        [SerializeField] int startingStone;
        [SerializeField] int startingFood;
        [SerializeField] int startingPoints;

        int wood;
        public int Wood
        {
            get
            {
                return wood;
            }
            set
            {
                wood = value;
                OnResourceUpdated?.Invoke();
            }
        }

        int stone;
        public int Stone
        {
            get
            {
                return stone;
            }
            set
            {
                stone = value;
                OnResourceUpdated?.Invoke();
            }
        }

        int food;
        public int Food
        {
            get
            {
                return food;
            }
            set
            {
                food = value;
                OnResourceUpdated?.Invoke();
            }
        }

        int points;
        public int Points
        {
            get
            {
                return points;
            }
            set
            {
                points = value;
                OnResourceUpdated?.Invoke();
            }
        }

        public void Initialize()
        {
            Wood = startingWood;
            Stone = startingStone;
            Food = startingFood;
            Points = startingPoints;
        }

        public bool CanCoverCastle
        {
            get { return Wood >= castleCost && Stone >= castleCost; }
        }

        public bool BuyCastle()
        {
            if (!CanCoverCastle) return false;

            Wood -= castleCost;
            Stone -= castleCost;
            return true;
        }

        public bool CanCoverFarm
        {
            get { return Wood >= farmCost; }
        }

        public bool BuyFarm()
        {
            if (!CanCoverFarm) return false;

            Wood -= farmCost;
            return true;
        }

        public bool CanCoverUnit
        {
            get { return Food >= unitUpkeep; }
        }

        public bool Won { get { return Points >= pointsForWin; } }

        public bool PayForUnit()
        {
            if (!CanCoverUnit) return false;

            Food -= unitUpkeep;
            return true;
        }

    }
}